package br.edu.iftm.pizzariadonmortandello.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.recyclerview.widget.RecyclerView;
import br.edu.iftm.pizzariadonmortandello.R;
import br.edu.iftm.pizzariadonmortandello.bean.Pizza;

public class PizzaAdapter extends RecyclerView.Adapter<PizzaAdapter.MyViewHolder> {

    private ArrayList<Pizza> pizzaList;

    public PizzaAdapter(ArrayList<Pizza> pizzaList) {
        this.pizzaList = pizzaList;
    }

    public static class MyViewHolder extends RecyclerView.ViewHolder {

        private boolean expanded;
        public LinearLayout expandListener;
        public TextView nome;
        public TextView preco;
        public TextView ingredientes;

        public MyViewHolder(View v) {
            super(v);
            expanded = false;
            expandListener = v.findViewById(R.id.recyclerview_pizza_item_listener);
            nome = v.findViewById(R.id.recyclerview_pizza_item_nome);
            preco = v.findViewById(R.id.recyclerview_pizza_item_preco);
            ingredientes = v.findViewById(R.id.recyclerview_pizza_item_ingredientes);
        }

        public void expand(){
            if(expanded){
                //RETRACT
                ingredientes.setVisibility(TextView.GONE);
                expanded = false;
            }else{
                //EXPAND
                ingredientes.setVisibility(TextView.VISIBLE);
                expanded = true;
            }
        }
    }

    @Override
    public PizzaAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent,
                                                     int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_pizza_item, parent, false);

        MyViewHolder vh = new MyViewHolder(v);
        return vh;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        //EXPAND ACCESS
        final MyViewHolder h = holder;
        //LISTENER
        holder.expandListener.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                h.expand();
            }
        });

        //NOME
        String nome = pizzaList.get(position).getNome();
        if(nome.length() >= 22){
            nome = nome.substring(0, 20) + "..";
        }
        holder.nome.setText(nome);

        //PRECO
        String preco = "R$" + pizzaList.get(position).getPreco();
        holder.preco.setText(preco);

        //INGREDIENTES
        holder.ingredientes.setText(pizzaList.get(position).getIngredientes());

    }

    @Override
    public int getItemCount() {
        return pizzaList.size();
    }
}

