package br.edu.iftm.pizzariadonmortandello.singleton;

import android.os.Build;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

import br.edu.iftm.pizzariadonmortandello.bean.Pizza;

public class PizzaSingleton {

    private static PizzaSingleton INSTANCE;

    private ArrayList<Pizza> pizzas;

    private PizzaSingleton(){
        pizzas = new ArrayList<Pizza>();
    }

    public static PizzaSingleton getInstance(){
        if(INSTANCE == null)INSTANCE = new PizzaSingleton();
        else{

        }
        return INSTANCE;
    }

    public ArrayList<Pizza> getPizzas(){
        return pizzas;
    }

    public void addPizza(Pizza in){
        pizzas.add(in);
        sort();
    }

    private void sort(){
        if(pizzas.size() > 1){
            Collections.sort(pizzas, new Comparator<Pizza>() {
                @Override
                public int compare(Pizza p1, Pizza p2) {
                    return p1.getNome().compareToIgnoreCase(p2.getNome());
                }
            });
        }
    }

}
