package br.edu.iftm.pizzariadonmortandello;

import androidx.appcompat.app.AppCompatActivity;
import br.edu.iftm.pizzariadonmortandello.bean.Pizza;
import br.edu.iftm.pizzariadonmortandello.singleton.PizzaSingleton;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;

public class NewPizzaActivity extends AppCompatActivity {

    private EditText nameField, priceField, ingredientField;

    private Button saveBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_newpizza);

        saveBtn = findViewById(R.id.newpizza_btn_save);

        nameField = findViewById(R.id.newpizza_field_name);
        priceField = findViewById(R.id.newpizza_field_price);
        ingredientField = findViewById(R.id.newpizza_field_ingredient);

        saveBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(nameField.getText().toString().trim().isEmpty()){
                    nameField.setError("Insira um nome");
                }else if(priceField.getText().toString().trim().isEmpty()){
                    priceField.setError("Insira um preco");
                }else if(ingredientField.getText().toString().trim().isEmpty()){
                    ingredientField.setError("Lista de ingredientes vazia");
                }else{
                    String name = nameField.getText().toString().trim();
                    double price = Double.parseDouble(priceField.getText().toString());
                    String ingredients = ingredientField.getText().toString().trim();

                    Pizza newPizza = new Pizza(name, price, ingredients);

                    PizzaSingleton.getInstance().addPizza(newPizza);

                    Intent pizzaTime = new Intent(NewPizzaActivity.this, MainActivity.class);
                    pizzaTime.putExtra("message", "Pizza " + newPizza.getNome() + " inserida com sucesso!");
                    startActivity(pizzaTime);
                }
            }
        });
    }
}
