package br.edu.iftm.pizzariadonmortandello;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import br.edu.iftm.pizzariadonmortandello.adapter.PizzaAdapter;
import br.edu.iftm.pizzariadonmortandello.bean.Pizza;
import br.edu.iftm.pizzariadonmortandello.singleton.PizzaSingleton;

import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

public class PizzaMenuActivity extends AppCompatActivity {

    private RecyclerView pizzaRecyclerView;
    private PizzaAdapter pizzaAdapter;
    private RecyclerView.LayoutManager pizzaLayoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pizzamenu);
        pizzaRecyclerView = findViewById(R.id.recyclerview_pizza);

        pizzaLayoutManager = new LinearLayoutManager(this);
        pizzaRecyclerView.setLayoutManager(pizzaLayoutManager);

        pizzaAdapter = new PizzaAdapter(PizzaSingleton.getInstance().getPizzas());
        pizzaRecyclerView.setAdapter(pizzaAdapter);
    }

}
