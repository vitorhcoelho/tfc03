package br.edu.iftm.pizzariadonmortandello.bean;

import android.os.Parcel;
import android.os.Parcelable;

public class Pizza{

    private String nome;
    private double preco;
    private String ingredientes;

    public Pizza(String nome, double preco, String ingredientes) {
        this.nome = nome;
        this.preco = preco;
        this.ingredientes = ingredientes;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getIngredientes() {
        return ingredientes;
    }

    public void setIngredientes(String ingredientes) {
        this.ingredientes = ingredientes;
    }

    @Override
    public String toString() {
        return "Pizza{" +
                "nome='" + nome + '\'' +
                ", preco=" + preco +
                ", ingredientes='" + ingredientes + '\'' +
                '}';
    }
}
