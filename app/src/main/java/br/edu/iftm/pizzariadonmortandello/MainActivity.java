package br.edu.iftm.pizzariadonmortandello;

import androidx.appcompat.app.AppCompatActivity;
import br.edu.iftm.pizzariadonmortandello.bean.Pizza;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private Button registerPizzaBtn, seePizzaBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        registerPizzaBtn = findViewById(R.id.main_btn_registerPizza);
        seePizzaBtn = findViewById(R.id.main_btn_seePizza);

        if(getIntent().getStringExtra("message") != null){
            Toast.makeText(MainActivity.this, getIntent().getStringExtra("message"), Toast.LENGTH_LONG).show();
        }

        registerPizzaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pizzaTime = new Intent(MainActivity.this, NewPizzaActivity.class);
                startActivity(pizzaTime);
            }
        });

        seePizzaBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent pizzaTime = new Intent(MainActivity.this, PizzaMenuActivity.class);
                startActivity(pizzaTime);
            }
        });
    }
}
